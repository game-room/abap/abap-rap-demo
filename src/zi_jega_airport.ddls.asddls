@AbapCatalog.viewEnhancementCategory: [#NONE]
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Definition for autocomplete airpot'
@Metadata.ignorePropagatedAnnotations: true
@ObjectModel.usageType:{
    serviceQuality: #X,
    sizeCategory: #S,
    dataClass: #MIXED
}
define view entity ZI_JEGA_AIRPORT
  as select from /dmo/airport
{
      @UI.lineItem: [{position: 10 }]
  key airport_id as AirportId,
      @UI.lineItem: [{position: 20 }]
      name       as Name
}
